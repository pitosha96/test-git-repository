package com.itransition.test;

public class Test5 {
    private static final int a = 0;

    public int test() {
        return a;
    }

    public int func() {
        return a;
    }
}
