package com.itransition.test;

public class Test4 {
    private static final int a = 0;

    public int test() {
        return a;
    }

    public int func() {
        return a;
    }

    public int func2() {
        return a;
    }
}
